package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"gitlab.com/Kejsty/cash-register/register"
)

const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ" +
	"abcdefghijklmnopqrstuvwxy" +
	"0123456789"

func main() {
	rand.Seed(time.Now().UnixNano())
	createPayment()
	createPayment()
	createPayment()
	getPayments()
}

func createPayment() {
	log.Println("ADD")
	body, err := json.Marshal(map[string]interface{}{
		"fik":        RandString(),
		"created_at": time.Now().Format(time.RFC3339),
		"amount":     4.5,
	})

	if err != nil {
		log.Fatal(err)
	}

	resp, err := http.Post("http://localhost:3000/api/add", "application/json", bytes.NewBuffer(body))
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(string(body))
}

func getPayments() {
	log.Println("GET")
	//first all
	resp, err := http.Get("http://localhost:3000/api/all")
	if err != nil {
		log.Fatal(err)
	}

	payments := []register.Payment{}

	err = json.NewDecoder(resp.Body).Decode(&payments)
	if err != nil {
		log.Fatal(err)
		return
	}

	if len(payments) != 3 {
		log.Println("Testting ", len(payments), " payments that were receivedthrough /api/all call")
	}
	resp.Body.Close()

	for _, p := range payments {
		var pById, pByFIK register.Payment

		resp, err := http.Get(fmt.Sprintf("http://localhost:3000/api/get?id=%d", p.ID))
		if err != nil {
			log.Fatal(err)
		}

		err = json.NewDecoder(resp.Body).Decode(&pById)
		if err != nil {
			log.Fatal(err)
			return
		}
		resp.Body.Close()

		resp, err = http.Get(fmt.Sprintf("http://localhost:3000/api/get?fik=%s", p.FIK))
		if err != nil {
			log.Fatal(err)
		}

		err = json.NewDecoder(resp.Body).Decode(&pByFIK)
		if err != nil {
			log.Fatal(err)
			return
		}
		resp.Body.Close()

		if pById != pByFIK {
			log.Printf("Got Payments are not equal: original %v, by id: %v, by fik: %v", p, pById, pByFIK)
		}
	}
}

func RandString() string {
	var b strings.Builder
	for i := 0; i < 10; i++ {
		b.WriteString(string(chars[rand.Intn(len(chars))]))
	}
	return b.String()
}
