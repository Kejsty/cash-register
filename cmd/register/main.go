package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/Kejsty/cash-register/register"
	"gitlab.com/Kejsty/cash-register/storage"
	"gitlab.com/Kejsty/cash-register/utils"
)

func main() {

	if os.Getenv("ENVIRONMENT") != "prod" {
		utils.YamlToEnv("./app.yaml")
	}
	//in prod, ENV is provided in deployment

	db, err := storage.NewPostgreWrapper()
	if err != nil {
		log.Println("Failed to open db: ", err)
		return
	}

	defer db.Close()

	mux := http.NewServeMux()
	var handl *register.CashRegisterHandler
	for repeat := 0; repeat < 5; repeat++ {
		handl, err = register.NewCashRegisterHandler(db)

		if err != nil {
			log.Println("Failed to create register: ", err)
			time.Sleep(time.Duration(repeat*3) * time.Second)
			continue
		}
	}

	if handl == nil {
		log.Println("Failed to connect to DB in 5 attempts")
		return
	}

	handl.InitializeMux(mux)

	port := utils.GetEnv("SERVER_PORT", ":3000")

	log.Println("Listnening on localhost", port)
	http.ListenAndServe(port, mux)
}
