package storage

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	"gitlab.com/Kejsty/cash-register/utils"
)

type DBWrapper struct {
	*sql.DB
}

//NewPostgreWrapper  ConnectDB : connecting DB
func NewPostgreWrapper() (*DBWrapper, error) {
	host := utils.GetEnv("DB_HOST", "127.0.0.1")
	port := utils.GetEnv("DB_PORT", "5432")
	user := utils.GetEnv("DB_USER", "postgres")
	dbName := utils.GetEnv("DB_DBNAME", "cashregister")
	pass := utils.GetEnv("DB_PASS", "postgre")

	db, err := sql.Open("postgres",
		fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
			host, port, user, dbName, pass))

	if err != nil {
		return nil, err
	}

	wrapper := &DBWrapper{db}
	return wrapper, nil
}
