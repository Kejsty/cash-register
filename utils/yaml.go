package utils

import (
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"
)

func YamlToEnv(path string) {
	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		log.Panicf("YamlToEnv err #%v ", err)
	}

	var envContainer map[interface{}]interface{}
	err = yaml.Unmarshal(yamlFile, &envContainer)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

	setEnv(envContainer)
}

func setEnv(input map[interface{}]interface{}) {
	log.Println("Yaml initialization :")
	for key, dataItem := range input {
		typeOfItem := reflect.ValueOf(dataItem).Kind()
		if typeOfItem == reflect.String || typeOfItem == reflect.Int {
			if typeOfItem == reflect.Int {
				dataItem = strconv.Itoa(dataItem.(int))
			}
			envKey := strings.ToUpper(key.(string))
			envValue := dataItem.(string)

			if os.Getenv(envKey) == "" { //skip if exists
				log.Printf("- setting env %s=%s", envKey, envValue)
				err := os.Setenv(envKey, envValue)
				if err != nil {
					log.Fatalf("SetEnv from yaml: %v", err)
				}
			}
		}
	}
}
