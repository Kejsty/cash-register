package utils

import (
	"fmt"
	"os"
)

//GetEnv get environment variable $key of type string or return $fallback
func GetEnv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		fmt.Printf("%s not specified, using default value %s\n", key, value)
		return fallback
	}
	return value
}
