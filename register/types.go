package register

import (
	"fmt"
	"time"
)

type Payment struct {
	ID        uint      `gorm:"primary_key" json:"id"`
	FIK       string    `gorm:"type:varchar(39);unique_index" json:"fik"`
	CreatedAt time.Time `gorm:"column:created_at" json:"created_at"`
	Amount    float64   `gorm:"column:amount" json:"amount"`
}

const createPaymentsTable string = `
CREATE TABLE IF NOT EXISTS payments  (
	id SERIAL PRIMARY KEY,
	fik TEXT UNIQUE,
	created_at DATE,
	amount TEXT
  );
`

const insertSQL string = "INSERT INTO payments(fik, created_at, amount) VALUES($1, $2, $3) RETURNING id"
const getOneByIDSQL string = "SELECT id, fik, created_at, amount from payments where id=$1"
const getOneByFIKSQL string = "SELECT id, fik, created_at, amount from payments where fik=$1"
const getAllSQL string = "SELECT id, fik, created_at, amount from payments"

//Validate validates given payment
func (p Payment) Validate() error {
	if len(p.FIK) == 0 {
		return fmt.Errorf("fik parameter missing")
	}

	if len(p.FIK) > 39 {
		return fmt.Errorf("FIK legnth should max max 39 characters")
	}

	if p.CreatedAt.Unix() == 0 {
		return fmt.Errorf("created at timestamp missing")
	}

	if p.Amount == 0 {
		return fmt.Errorf("amount missing")
	}

	return nil
}
