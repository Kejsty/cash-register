package register

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	"gitlab.com/Kejsty/cash-register/storage"
)

type CashRegister struct {
	db *storage.DBWrapper
}

//NewCashRegister creates new CashRegister connected to provided db
func NewCashRegister(db *storage.DBWrapper) (*CashRegister, error) {
	if err := db.Ping(); err != nil {
		return nil, err
	}

	if _, err := db.Exec(createPaymentsTable); err != nil {
		return nil, err
	}

	return &CashRegister{
		db: db,
	}, nil
}

//CreatePayment creates new payments in DB
func (cr *CashRegister) CreatePayment(p Payment) (Payment, error) {
	var id uint
	err := cr.db.QueryRow(insertSQL, p.FIK, p.CreatedAt, p.Amount).Scan(&id)
	if err != nil && strings.Contains(err.Error(), "duplicate key value") {
		return p, fmt.Errorf("payment with %s already exists", p.FIK)
	}
	p.ID = id
	return p, err
}

//GetPaymentById finds payment in DB which meets given id
func (cr *CashRegister) GetPaymentById(id uint) (Payment, error) {
	row := cr.db.QueryRow(getOneByIDSQL, id)
	var p Payment

	switch err := row.Scan(&p.ID, &p.FIK, &p.CreatedAt, &p.Amount); err {
	case sql.ErrNoRows:
		return p, fmt.Errorf("payment with ID %d not found", id)
	case nil:
		return p, nil
	default:
		return p, err
	}
}

//GetPaymentByFIK finds payment in DB which meets given FIK
func (cr *CashRegister) GetPaymentByFIK(fik string) (Payment, error) {
	row := cr.db.QueryRow(getOneByFIKSQL, fik)
	var p Payment

	switch err := row.Scan(&p.ID, &p.FIK, &p.CreatedAt, &p.Amount); err {
	case sql.ErrNoRows:
		return p, fmt.Errorf("payment with FIK %d not found", fik)
	case nil:
		return p, nil
	default:
		return p, err
	}
}

//GetAllPayments lists all payments stored in DB
func (cr *CashRegister) GetAllPayments() ([]Payment, error) {
	rows, err := cr.db.Query(getAllSQL)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	payments := []Payment{}

	for rows.Next() {
		var p Payment
		if err := rows.Scan(&p.ID, &p.FIK, &p.CreatedAt, &p.Amount); err != nil {
			log.Println(err)
		} else {
			payments = append(payments, p)
		}
	}

	return payments, rows.Err()
}
