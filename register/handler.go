package register

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/Kejsty/cash-register/storage"
)

type CashRegisterHandler struct {
	r *CashRegister
}

//NewCashRegisterHandler create new CashRegisterHandler by creating CashRegister
func NewCashRegisterHandler(db *storage.DBWrapper) (*CashRegisterHandler, error) {
	cr, err := NewCashRegister(db)
	if err != nil {
		return nil, err
	}

	return &CashRegisterHandler{
		r: cr,
	}, nil
}

//InitializeMux inidializes HandleFunc callbacks on mux
func (crh CashRegisterHandler) InitializeMux(sm *http.ServeMux) {
	sm.HandleFunc("/api/add", crh.AddPayment)
	sm.HandleFunc("/api/get", crh.GetPayment)
	sm.HandleFunc("/api/all", crh.GetAllPayments)
}

//AddPayment creates new payments in DB
func (crh CashRegisterHandler) AddPayment(w http.ResponseWriter, r *http.Request) {
	log.Printf("Add New Payment\n")

	if r.Method != "POST" {
		fmt.Fprintf(w, "only POST methods are supported for adding payment")
	}

	var p Payment

	// Try to decode the request body into the struct. If there is an error,
	// respond to the client with the error message and a 400 status code.
	err := json.NewDecoder(r.Body).Decode(&p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := p.Validate(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	created, err := crh.r.CreatePayment(p)
	log.Printf("Added new Payment: %v\n", created)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(200)
	b, err := json.Marshal(created)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

//GetPayment gets payment based on provided query
func (crh CashRegisterHandler) GetPayment(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		fmt.Fprintf(w, "only POST methods are supported for getting payment info")
	}

	if len(r.URL.Query().Get("id")) != 0 {
		crh.GetPaymentById(w, r)
		return // handled
	}

	if len(r.URL.Query().Get("fik")) != 0 {
		crh.GetPaymentByFIK(w, r)
		return // handler
	}

	http.Error(w, "provide either id or fik", http.StatusBadRequest)
}

//GetPaymentById finds payment by id key (from query)
func (crh CashRegisterHandler) GetPaymentById(w http.ResponseWriter, r *http.Request) {
	log.Printf("Get Payment By Id: %s\n", r.URL.Query().Get("id"))
	if r.Method != "GET" {
		fmt.Fprintf(w, "only POST methods are supported for getting payment info")
	}

	id, err := strconv.ParseInt(r.URL.Query().Get("id"), 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	loaded, err := crh.r.GetPaymentById(uint(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	log.Printf("Response: %v\n", loaded)
	b, err := json.Marshal(loaded)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(b)
}

//GetPaymentByFIK finds payment by FIK key  (from query)
func (crh CashRegisterHandler) GetPaymentByFIK(w http.ResponseWriter, r *http.Request) {
	log.Printf("Get Payment By FIK: %s\n", r.URL.Query().Get("id"))

	if r.Method != "GET" {
		fmt.Fprintf(w, "only POST methods are supported for getting payment info")
	}

	loaded, err := crh.r.GetPaymentByFIK(r.URL.Query().Get("fik"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	log.Printf("Response: %v\n", loaded)
	b, err := json.Marshal(loaded)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(b)
}

//GetAllPayments handles /api/all request and returns all payments in DB
func (crh CashRegisterHandler) GetAllPayments(w http.ResponseWriter, r *http.Request) {
	log.Printf("Get All Payments\n")
	if r.Method != "GET" {
		fmt.Fprintf(w, "only POST methods are supported for getting all payments info")
	}

	loaded, err := crh.r.GetAllPayments()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Printf("Response: %v\n", loaded)
	b, err := json.Marshal(loaded)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(b)
}
