all: buildDocker runDB runServer

runDB: 
	-mkdir registerDB
	docker run --name registerDB -e POSTGRES_PASSWORD=postgre -e POSTGRES_USER=postgres -e POSTGRES_DB=cashregister -d -p 5432:5432 -v $(shell pwd)/registerDB:/var/lib/postgresql/data  postgres
	sleep 5

cleanup:
	-docker kill cashRegister
	-docker rm cashRegister
	-docker kill registerDB
	-docker rm registerDB
	-sudo rm -rf registerDB
	

buildDocker:
	docker build -f server.Dockerfile -t kejsty/cash-register .

runServer:
	docker run --name cashRegister -e ENVIRONMENT=prod  --env-file ./env.yaml -d --network host kejsty/cash-register

runClient:
	cd cmd/client && go run main.go