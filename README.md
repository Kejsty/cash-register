# Cash Register

### Setup

To setup server, go to the root directory of the project and type:

```
make
```

The setup is divided into three steps:
- build docker image ```make buildDocker```
- run postgre in docker ```make runDB```
- run server in docker ```make runServer```

Note that even thought the server attepts to connect to postgres multiple time, on slow computers it may not be enough. In this case you'll have to restart the server.

### Cleanup

To clean directory, simply type:

```
make cleanup
```

This command will kill docker containers, remove them, and remove local storage of postgreSQL. Note that due to changes of rwx privileges, you'll need sudo to remove the local storage of postgreSQL.

## API

The server provides payment creation, getting all or invidual payment.

### ADD payment

- POST `/api/add`
- Expects body in form of json, with parameters FIK( Fiskální identifikační kód ), created at timestamp (in RFC 3339 form) and amount.
Note that FIK must be unique. 

Example: 
```
{
	"fik":"sdu-dasijd9-dsiaodh-as",
	"created_at": "2011-08-30T13:22:53.108Z",
	"amount": 40.5
}
```

### GET payment

You can list payment by id or by fik
- GET `/api/get?id=${ID}`
- GET `/api/get?fik=${FIK}`

Example of response;
```
{
  "id":1,
	"fik":"sdu-dasijd9-dsiaodh-as",
	"created_at": "2011-08-30T13:22:53.108Z",
	"amount": 40.5
}
```


### GET all payment

- GET `/api/all`


Example of response;
```
[
  {
    "id": 1,
    "fik": "sdu-dasijd9-dsiaodh-as",
    "created_at": "2011-08-30T00:00:00Z",
    "amount": 40.5
  }
]
```
