module gitlab.com/Kejsty/cash-register

go 1.15

require (
	github.com/lib/pq v1.8.0
	gopkg.in/yaml.v2 v2.3.0
)
