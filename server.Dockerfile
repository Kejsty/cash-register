FROM golang:1.13.7-alpine3.10 AS build-env

WORKDIR /go/src/app
ENV GOPATH=/go
ADD . .

WORKDIR /go/src/app/cmd/register

RUN GOOS=linux CGO_ENABLED=0 go build -o /go/bin/api

FROM alpine:3.11

WORKDIR /app
COPY --from=build-env /go/bin/api /app/

ENTRYPOINT ["/app/api"]